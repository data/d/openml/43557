# OpenML dataset: Avocado-Prices-(Augmented)

https://www.openml.org/d/43557

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
It is a well known fact that Millenials LOVE Avocado Toast. It's also a well known fact that all Millenials live in their parents basements.
Clearly, they aren't buying home because they are buying too much Avocado Toast!
But maybe there's hope if a Millenial could find a city with cheap avocados, they could live out the Millenial American Dream.
Content
This data was downloaded from the Hass Avocado Board website in May of 2018  compiled into a single CSV. Here's how the Hass Avocado Board describes the data on their website:
The table below represents weekly 2018 retail scan data for National retail volume (units) and price. Retail scan data comes directly from retailers cash registers based on actual retail sales of Hass avocados. Starting in 2013, the table below reflects an expanded, multi-outlet retail data set. Multi-outlet reporting includes an aggregation of the following channels: grocery, mass, club, drug, dollar and military. The Average Price (of avocados) in the table reflects a per unit (per avocado) cost, even when multiple units (avocados) are sold in bags. The Product Lookup codes (PLUs) in the table are only for Hass avocados. Other varieties of avocados (e.g. greenskins) are not included in this table.
Some relevant columns in the dataset:
Date - The date of the observation
AveragePrice - the average price of a single avocado
type - conventional or organic
year - the year
Region - the city or region of the observation
Total Volume - Total number of avocados sold
4046 - Total number of avocados with PLU 4046 sold
4225 - Total number of avocados with PLU 4225 sold
4770 - Total number of avocados with PLU 4770 sold
Acknowledgements
Many thanks to the Hass Avocado Board for sharing this data!!
http://www.hassavocadoboard.com/retail/volume-and-price-data
Inspiration
In which cities can millenials have their avocado toast AND buy a home?
Was the Avocadopocalypse of 2017 real?
NEW Augmented Data with CTGAN

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43557) of an [OpenML dataset](https://www.openml.org/d/43557). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43557/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43557/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43557/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

